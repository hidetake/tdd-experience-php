<?php
namespace rental;

class MovieRentalType
{
    const NEW_RELEASE = 1;
    const REGULAR = 2;
    const KIDS = 3;
}
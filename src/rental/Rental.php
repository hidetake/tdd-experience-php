<?php
namespace rental;

class Rental
{
    /**
     * @var Movie
     */
    public $movie;

    /**
     * @var int
     */
    public $daysRented;

    /**
     * Rental constructor.
     * @param Movie $movie
     * @param int $daysRented
     */
    public function __construct($movie, $daysRented)
    {
        $this->movie = $movie;
        $this->daysRented = $daysRented;
    }
}
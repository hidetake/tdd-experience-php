<?php

require_once __DIR__ . '/rental/Movie.php';
require_once __DIR__ . '/rental/MovieRentalType.php';
require_once __DIR__ . '/rental/Rental.php';
require_once __DIR__ . '/rental/Customer.php';

use Slim\Http\Request;
use Slim\Http\Response;
use rental\Movie;
use rental\MovieRentalType;
use rental\Rental;
use rental\Customer;

// Routes

$movies = array(
    'swiv' => new Movie("STAR WARS IV", MovieRentalType::REGULAR),
    'solo' => new Movie("ハン・ソロ", MovieRentalType::NEW_RELEASE),
    'thumb' => new Movie("親指スターウォーズ", MovieRentalType::KIDS),
);

$app->get('/', function (Request $request, Response $response) use ($movies) {
    // Sample log message
    $this->logger->info("'/' route");

    $customer = new Customer("taro");

    $rentals = isset($_SESSION["rentals"]) ?  $_SESSION["rentals"] : array();

    foreach ($rentals as $item) {
        $customer->add(new Rental($movies[$item["movie"]], $item["quantity"]));
    }

    // Render index view
    return $this->renderer->render($response, 'index.phtml', array('movies' => $movies, 'rentals' => $rentals, 'price' => $customer->statement()));
});

$app->post('/add', function (Request $request, Response $response) {
    $this->logger->info("'/add' route");

    $key = $request->getParam("movie");
    $quantity = $request->getParam("quantity");

    $rentals = isset($_SESSION["rentals"]) ?  $_SESSION["rentals"] : array();
    array_push($rentals, array("movie" => $key, "quantity" => $quantity));
    $_SESSION["rentals"] = $rentals;

    return $response->withRedirect('/');
});

$app->post('/clear', function (Request $request, Response $response) {
    $this->logger->info("'/clear' route");

    $_SESSION["rentals"] = array();

    return $response->withRedirect('/');
});